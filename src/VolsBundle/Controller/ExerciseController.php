<?php

namespace VolsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use VolsBundle\Entity\Ville;
use VolsBundle\Entity\Aeroport;
use VolsBundle\Entity\Vol;
use Symfony\Component\HttpFoundation\Response;
use VolsBundle\Entity\InfosEscale;
use DateTime;
use VolsBundle\Entity\Client;
use VolsBundle\Entity\Reservation;


class ExerciseController extends Controller{
    
    public function insererVilleAction(){
        
        $ville=new Ville();
        $ville->setNom("Moscow");
        $em=$this->getDoctrine()->getManager();
        $em->persist($ville);
        $em->flush();
//        return new Response("The city has been inserted.");
        
        return $this-> render("VolsBundleViews/ModeleViews/insererVille.html.twig");
    }
    
    public function afficherVillesAction(){
        $em=$this->getDoctrine()->getManager(); 
        $rep=$em->getRepository("VolsBundle\Entity\Ville");
        $villes=$rep->findAll();
        $vars=['villes'=>$villes,
            'nom'=>'Daiva',
            'lotto'=>[2,5,8]];
      
    return $this-> render("VolsBundleViews/ModeleViews/afficherVilles.html.twig", $vars);   
    }
    
    public function insererAeroportAction(){
      
        $em=$this->getDoctrine()->getManager();
        $aeroport1=new Aeroport();
        $aeroport1->setNom("AirportMoscow");
        $aeroport1->setCode("2586");
        
        $aeroport2=new Aeroport();
        $aeroport2->setNom("AeroportSmall");
        $aeroport2->setCode("3654");
//        surandam miesta
        $rep=$em->getRepository("VolsBundle\Entity\Ville");
        $ville=$rep->findOneBy(array('nom'=>"Moscow"));
        
//        pridedam miesta prie oro uostu:
         
        $aeroport1->setVille($ville);       
        $aeroport2->setVille($ville);
//        cia pridedam oro uosta prie miesto, bet mes to nematot DB
        $ville->addAeroport($aeroport1);
        $ville->addAeroport($aeroport2);
        
        
        $em->persist($ville); 
               
        $em->flush();
    return $this-> render("VolsBundleViews/ModeleViews/insererAeroport.html.twig");
    }
    
    public function deleteAeroportAction(){
        $em=$this->getDoctrine()->getManager();
        $rep=$em->getRepository("VolsBundle\Entity\Aeroport");
        $aeroport=$rep->findAll();
        $nbr=count($aeroport);
        $dernierAero=$aeroport[$nbr-1];
        $em->remove ($dernierAero);
        $em->flush();
    return $this-> render("VolsBundleViews/ModeleViews/deleteAeroport.html.twig");
    }
    
    public function afficherVillesEtAeroportsAction(){
        $em=$this->getDoctrine()->getManager();
        $rep=$em->getRepository("VolsBundle\Entity\Ville");
        $villes=$rep->findAll();
//        dump($villes);
//        $aeroportsDePremierVille=$villes[0]->getAeroports();
//        dump($aeroportsDePremierVille[1]);
        $vars=['villes'=>$villes];
//      return new Response();
    return $this-> render("VolsBundleViews/ModeleViews/afficherVillesEtAeroports.html.twig", $vars);   
    }
    
    public function deleteVilleAvecAeroportAction(){
        $em=$this->getDoctrine()->getManager();
        $rep1=$em->getRepository("VolsBundle\Entity\Ville");
        $ville=$rep1->findOneBy(array('nom'=>'Moscow'));     
        $em->remove ($ville);
        $em->flush();
     return $this-> render("VolsBundleViews/ModeleViews/deleteVilleAvecAeroport.html.twig");    
    }
    
//    Créez une action qui affiche les deux vols et le nom de la ville de départ et de la ville d'arrivée 
    
     public function afficherVolsAvecVilleAction(){
         $em=$this->getDoctrine()->getManager();
         $rep=$em->getRepository(Vol::class);
         $vols=$rep->findAll();       
         $vars=['vols'=>$vols];
         return $this->render("VolsBundleViews/ModeleViews/afficherVolsAvecVille.html.twig", $vars);
     }
     
     public function insererVolAction(){
         $em=$this->getDoctrine()->getManager();
         $vol=new Vol();
         $vol->setNumero(2589);
         // date_create
         $vol->setDateDepart(new DateTime("01/05/2018 15:15:00"));
         $vol->setDateArrive(new DateTime("01/05/2018 23:15:00"));        
//         $vol->setHeureDepart(new DateTime("15:15:00")); 
//         $vol->setHeureArrive(new DateTime("23:15:00"));
         
         $rep1=$em->getRepository(Aeroport::class);
         $aeroport1=$rep1->findOneBy(array('nom'=>"AeroportSmall"));
         $rep2=$em->getRepository(Aeroport::class);
         $aeroport2=$rep2->findOneBy(array('nom'=>"Nice Aeroport"));
//         pridedam oro uosta prie skrydzio (add nereikia)
  
         $vol->setAeroportDepart($aeroport1);
         $vol->setAeroportArrivee($aeroport2);
         
         $em->persist($vol);

         $em->flush();
         
      return $this->render("VolsBundleViews/ModeleViews/insererVol.html.twig");
     }
     
     public function insererVolAvecEscalesAction(){
         $em=$this->getDoctrine()->getManager();
         $vol=new Vol();
         $vol->setNumero(2546);
         $vol->setDateDepart(new DateTime("01/08/2018 10:15:00"));
         $vol->setDateArrive(new DateTime("01/08/2018 15:15:00")); 
         
         $rep=$em->getRepository(Aeroport::class);
         $aeroport1=$rep->findOneBy(array('nom'=>"AeroportSmall"));
         $aeroport2=$rep->findOneBy(array('nom'=>"Nice Aeroport"));
         
         $vol->setAeroportDepart($aeroport1);
         $vol->setAeroportArrivee($aeroport2); 
         
         $escale=new InfosEscale();
         $escale->setDuree(2,15);
         $escale->setDateArrive(new DateTime("01/08/2018 11:15:00"));
         $escale->setDateDepart(new DateTime("01/08/2018 12:25:00"));
                           
         $vol->addInfosEscale($escale); 
         $escale->setVol($vol);         
         
         $em->persist($vol);

         $em->flush();
         
      return $this->render("VolsBundleViews/ModeleViews/insererVolAvecEscales.html.twig");  
         
     }
     
     public function afficherVolAvecEscaleAction(){
         $em=$this->getDoctrine()->getManager();
         $rep=$em->getRepository(Vol::class);
         $vol=$rep->findOneBy(array('numero'=>'2546'));
         $vars=['unVol'=>$vol];
         
     return $this->render("VolsBundleViews/ModeleViews/afficherVolAvecEscale.html.twig", $vars);    
     }
     
      public function afficherAeroportsDuneVilleAction(){
          $em=$this->getDoctrine()->getManager();
          $rep=$em->getRepository(Ville::class);
          $ville=$rep->findOneBy(array('nom'=>'Moscow'));
          $vars=['ville'=>$ville];
     return $this->render("VolsBundleViews/ModeleViews/afficherAeroportsDuneVille.html.twig", $vars);          
      }
      
     public function insererClientAction(){
         $em=$this->getDoctrine()->getManager();
         $rep=$em->getRepository(Client::class);
         $client=new Client();
         $client->setAdresse("Rue Marcel, 13");
         $client->setNom("Hulo");
         $client->setPrenom("John");
         $client->setNumTel("2232222");
         
         $em->persist($client);
         $em->flush();
          
    return $this->render("VolsBundleViews/ModeleViews/insererClient.html.twig");    
     }
     public function aficherClientsAction(){
         $em=$this->getDoctrine()->getManager();
         $rep=$em->getRepository(Client::class);
         $lesClients=$rep->findAll();
         $vars=["clients"=>$lesClients];
         
    return $this->render("VolsBundleViews/ModeleViews/aficherClients.html.twig", $vars);          
     }
     public function insererResAuClientsAction(){
         $em=$this->getDoctrine()->getManager();
         $reservation=new Reservation();
         $reservation->setDate(new DateTime("01/06/2018"));
         $reservation->setNumero(12);
           
         $reservation1=new Reservation();
         $reservation1->setDate(new DateTime("01/06/2017"));
         $reservation1->setNumero(13);
         
         $rep=$em->getRepository(Client::class);
         $client1=$rep->findOneby(array('id'=>'1'));
         $client2=$rep->findOneby(array('id'=>'2'));
         
         $client1->addReservation($reservation);
         $client2->addReservation($reservation1);
         
         $reservation->setClient($client1);
         $reservation1->setClient($client2);
         
         $em->persist($client1);
         $em->persist($client2);
         
         $em->flush();
         
    return $this->render("VolsBundleViews/ModeleViews/insererResAuClients.html.twig");     
     }
        
}

