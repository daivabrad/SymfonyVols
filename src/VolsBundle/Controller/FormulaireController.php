<?php

namespace VolsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use VolsBundle\Entity\Client;
use Symfony\Component\HttpFoundation\Response;
use VolsBundle\Form\ClientType;
use Symfony\Component\HttpFoundation\Request;

class FormulaireController extends Controller{
    
    public function afficherFormClientAction(){
        $client=new Client();
        $formClient=$this->createForm("VolsBundle\Form\ClientType", $client,  
                array('action'=>$this->generateUrl("treatmentFormClient"),
                'method'=>'POST'));
        
        $vars=['myFormClient' => $formClient->createView()];
        return $this->render('VolsBundleViews\FormViews\afficherFormClient.html.twig', $vars);
    }
    
    public function treatmentFormClientAction(Request $req){
        $formClient=$this->createForm(ClientType::class);
        $formClient->handleRequest($req);
        if($formClient->isSubmitted() && $formClient->isValid()){
            dump($formClient->getData());
            $client=$formClient->getData();
//            dump($client);
            $em=$this->getDoctrine()->getManager(); 
            $em->persist($client);
            $em->flush();
        }
       return new Response("treatment is ok"); 
    }
}

