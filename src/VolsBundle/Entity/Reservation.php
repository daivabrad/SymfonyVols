<?php

namespace VolsBundle\Entity;

/**
 * Reservation
 */
class Reservation
{
    /**
     * @var integer
     */
    private $numero;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \VolsBundle\Entity\Vol
     */
    private $Vol;

    /**
     * @var \VolsBundle\Entity\Passanger
     */
    private $Passanger;

    /**
     * @var \VolsBundle\Entity\Client
     */
    private $Client;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $passagers;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->passagers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Reservation
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Reservation
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vol
     *
     * @param \VolsBundle\Entity\Vol $vol
     *
     * @return Reservation
     */
    public function setVol(\VolsBundle\Entity\Vol $vol = null)
    {
        $this->Vol = $vol;

        return $this;
    }

    /**
     * Get vol
     *
     * @return \VolsBundle\Entity\Vol
     */
    public function getVol()
    {
        return $this->Vol;
    }

    /**
     * Set passanger
     *
     * @param \VolsBundle\Entity\Passanger $passanger
     *
     * @return Reservation
     */
    public function setPassanger(\VolsBundle\Entity\Passanger $passanger = null)
    {
        $this->Passanger = $passanger;

        return $this;
    }

    /**
     * Get passanger
     *
     * @return \VolsBundle\Entity\Passanger
     */
    public function getPassanger()
    {
        return $this->Passanger;
    }

    /**
     * Set client
     *
     * @param \VolsBundle\Entity\Client $client
     *
     * @return Reservation
     */
    public function setClient(\VolsBundle\Entity\Client $client = null)
    {
        $this->Client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \VolsBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->Client;
    }

    /**
     * Add passager
     *
     * @param \VolsBundle\Entity\passanger $passager
     *
     * @return Reservation
     */
    public function addPassager(\VolsBundle\Entity\passanger $passager)
    {
        $this->passagers[] = $passager;

        return $this;
    }

    /**
     * Remove passager
     *
     * @param \VolsBundle\Entity\passanger $passager
     */
    public function removePassager(\VolsBundle\Entity\passanger $passager)
    {
        $this->passagers->removeElement($passager);
    }

    /**
     * Get passagers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPassagers()
    {
        return $this->passagers;
    }
}
