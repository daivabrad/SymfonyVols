<?php

namespace VolsBundle\Entity;

/**
 * Ville
 */
class Ville
{
    /**
     * @var string
     */
    private $nom;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $aeroports;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->aeroports = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Ville
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add aeroport
     *
     * @param \VolsBundle\Entity\Aeroport $aeroport
     *
     * @return Ville
     */
    public function addAeroport(\VolsBundle\Entity\Aeroport $aeroport)
    {
        $this->aeroports[] = $aeroport;

        return $this;
    }

    /**
     * Remove aeroport
     *
     * @param \VolsBundle\Entity\Aeroport $aeroport
     */
    public function removeAeroport(\VolsBundle\Entity\Aeroport $aeroport)
    {
        $this->aeroports->removeElement($aeroport);
    }

    /**
     * Get aeroports
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAeroports()
    {
        return $this->aeroports;
    }
}
