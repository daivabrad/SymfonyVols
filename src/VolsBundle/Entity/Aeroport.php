<?php

namespace VolsBundle\Entity;

/**
 * Aeroport
 */
class Aeroport
{
    /**
     * @var string
     */
    private $nom;

    /**
     * @var integer
     */
    private $code;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $infosEscale;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $volsDepart;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $volsArrivee;

    /**
     * @var \VolsBundle\Entity\Ville
     */
    private $ville;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->infosEscale = new \Doctrine\Common\Collections\ArrayCollection();
        $this->volsDepart = new \Doctrine\Common\Collections\ArrayCollection();
        $this->volsArrivee = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Aeroport
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set code
     *
     * @param integer $code
     *
     * @return Aeroport
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return integer
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add infosEscale
     *
     * @param \VolsBundle\Entity\InfosEscale $infosEscale
     *
     * @return Aeroport
     */
    public function addInfosEscale(\VolsBundle\Entity\InfosEscale $infosEscale)
    {
        $this->infosEscale[] = $infosEscale;

        return $this;
    }

    /**
     * Remove infosEscale
     *
     * @param \VolsBundle\Entity\InfosEscale $infosEscale
     */
    public function removeInfosEscale(\VolsBundle\Entity\InfosEscale $infosEscale)
    {
        $this->infosEscale->removeElement($infosEscale);
    }

    /**
     * Get infosEscale
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInfosEscale()
    {
        return $this->infosEscale;
    }

    /**
     * Add volsDepart
     *
     * @param \VolsBundle\Entity\Vol $volsDepart
     *
     * @return Aeroport
     */
    public function addVolsDepart(\VolsBundle\Entity\Vol $volsDepart)
    {
        $this->volsDepart[] = $volsDepart;

        return $this;
    }

    /**
     * Remove volsDepart
     *
     * @param \VolsBundle\Entity\Vol $volsDepart
     */
    public function removeVolsDepart(\VolsBundle\Entity\Vol $volsDepart)
    {
        $this->volsDepart->removeElement($volsDepart);
    }

    /**
     * Get volsDepart
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVolsDepart()
    {
        return $this->volsDepart;
    }

    /**
     * Add volsArrivee
     *
     * @param \VolsBundle\Entity\Vol $volsArrivee
     *
     * @return Aeroport
     */
    public function addVolsArrivee(\VolsBundle\Entity\Vol $volsArrivee)
    {
        $this->volsArrivee[] = $volsArrivee;

        return $this;
    }

    /**
     * Remove volsArrivee
     *
     * @param \VolsBundle\Entity\Vol $volsArrivee
     */
    public function removeVolsArrivee(\VolsBundle\Entity\Vol $volsArrivee)
    {
        $this->volsArrivee->removeElement($volsArrivee);
    }

    /**
     * Get volsArrivee
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVolsArrivee()
    {
        return $this->volsArrivee;
    }

    /**
     * Set ville
     *
     * @param \VolsBundle\Entity\Ville $ville
     *
     * @return Aeroport
     */
    public function setVille(\VolsBundle\Entity\Ville $ville = null)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return \VolsBundle\Entity\Ville
     */
    public function getVille()
    {
        return $this->ville;
    }
}
