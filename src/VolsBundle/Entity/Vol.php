<?php

namespace VolsBundle\Entity;

/**
 * Vol
 */
class Vol
{
    /**
     * @var integer
     */
    private $numero;

    /**
     * @var \DateTime
     */
    private $dateDepart;

    /**
     * @var \DateTime
     */
    private $dateArrive;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $companies;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $reservations;

    /**
     * @var \VolsBundle\Entity\Aeroport
     */
    private $aeroportDepart;

    /**
     * @var \VolsBundle\Entity\Aeroport
     */
    private $aeroportArrivee;

    /**
     * @var \VolsBundle\Entity\Companie
     */
    private $companie;

    /**
     * @var \VolsBundle\Entity\InfosEscale
     */
    private $infosEscale;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companies = new \Doctrine\Common\Collections\ArrayCollection();
        $this->reservations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set numero
     *
     * @param integer $numero
     *
     * @return Vol
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set dateDepart
     *
     * @param \DateTime $dateDepart
     *
     * @return Vol
     */
    public function setDateDepart($dateDepart)
    {
        $this->dateDepart = $dateDepart;

        return $this;
    }

    /**
     * Get dateDepart
     *
     * @return \DateTime
     */
    public function getDateDepart()
    {
        return $this->dateDepart;
    }

    /**
     * Set dateArrive
     *
     * @param \DateTime $dateArrive
     *
     * @return Vol
     */
    public function setDateArrive($dateArrive)
    {
        $this->dateArrive = $dateArrive;

        return $this;
    }

    /**
     * Get dateArrive
     *
     * @return \DateTime
     */
    public function getDateArrive()
    {
        return $this->dateArrive;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add company
     *
     * @param \VolsBundle\Entity\Companie $company
     *
     * @return Vol
     */
    public function addCompany(\VolsBundle\Entity\Companie $company)
    {
        $this->companies[] = $company;

        return $this;
    }

    /**
     * Remove company
     *
     * @param \VolsBundle\Entity\Companie $company
     */
    public function removeCompany(\VolsBundle\Entity\Companie $company)
    {
        $this->companies->removeElement($company);
    }

    /**
     * Get companies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * Add reservation
     *
     * @param \VolsBundle\Entity\Reservation $reservation
     *
     * @return Vol
     */
    public function addReservation(\VolsBundle\Entity\Reservation $reservation)
    {
        $this->reservations[] = $reservation;

        return $this;
    }

    /**
     * Remove reservation
     *
     * @param \VolsBundle\Entity\Reservation $reservation
     */
    public function removeReservation(\VolsBundle\Entity\Reservation $reservation)
    {
        $this->reservations->removeElement($reservation);
    }

    /**
     * Get reservations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReservations()
    {
        return $this->reservations;
    }

    /**
     * Set aeroportDepart
     *
     * @param \VolsBundle\Entity\Aeroport $aeroportDepart
     *
     * @return Vol
     */
    public function setAeroportDepart(\VolsBundle\Entity\Aeroport $aeroportDepart = null)
    {
        $this->aeroportDepart = $aeroportDepart;

        return $this;
    }

    /**
     * Get aeroportDepart
     *
     * @return \VolsBundle\Entity\Aeroport
     */
    public function getAeroportDepart()
    {
        return $this->aeroportDepart;
    }

    /**
     * Set aeroportArrivee
     *
     * @param \VolsBundle\Entity\Aeroport $aeroportArrivee
     *
     * @return Vol
     */
    public function setAeroportArrivee(\VolsBundle\Entity\Aeroport $aeroportArrivee = null)
    {
        $this->aeroportArrivee = $aeroportArrivee;

        return $this;
    }

    /**
     * Get aeroportArrivee
     *
     * @return \VolsBundle\Entity\Aeroport
     */
    public function getAeroportArrivee()
    {
        return $this->aeroportArrivee;
    }

    /**
     * Set companie
     *
     * @param \VolsBundle\Entity\Companie $companie
     *
     * @return Vol
     */
    public function setCompanie(\VolsBundle\Entity\Companie $companie = null)
    {
        $this->companie = $companie;

        return $this;
    }

    /**
     * Get companie
     *
     * @return \VolsBundle\Entity\Companie
     */
    public function getCompanie()
    {
        return $this->companie;
    }

    /**
     * Set infosEscale
     *
     * @param \VolsBundle\Entity\InfosEscale $infosEscale
     *
     * @return Vol
     */
    public function setInfosEscale(\VolsBundle\Entity\InfosEscale $infosEscale = null)
    {
        $this->infosEscale = $infosEscale;

        return $this;
    }

    /**
     * Get infosEscale
     *
     * @return \VolsBundle\Entity\InfosEscale
     */
    public function getInfosEscale()
    {
        return $this->infosEscale;
    }

    /**
     * Add infosEscale
     *
     * @param \VolsBundle\Entity\InfosEscale $infosEscale
     *
     * @return Vol
     */
    public function addInfosEscale(\VolsBundle\Entity\InfosEscale $infosEscale)
    {
        $this->infosEscale[] = $infosEscale;

        return $this;
    }

    /**
     * Remove infosEscale
     *
     * @param \VolsBundle\Entity\InfosEscale $infosEscale
     */
    public function removeInfosEscale(\VolsBundle\Entity\InfosEscale $infosEscale)
    {
        $this->infosEscale->removeElement($infosEscale);
    }
}
