<?php

namespace VolsBundle\Entity;

/**
 * Companie
 */
class Companie
{
    /**
     * @var string
     */
    private $nom;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $vols;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->vols = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Companie
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add vol
     *
     * @param \VolsBundle\Entity\Vol $vol
     *
     * @return Companie
     */
    public function addVol(\VolsBundle\Entity\Vol $vol)
    {
        $this->vols[] = $vol;

        return $this;
    }

    /**
     * Remove vol
     *
     * @param \VolsBundle\Entity\Vol $vol
     */
    public function removeVol(\VolsBundle\Entity\Vol $vol)
    {
        $this->vols->removeElement($vol);
    }

    /**
     * Get vols
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVols()
    {
        return $this->vols;
    }
}
